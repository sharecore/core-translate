<?php

namespace core\translate\helper;

class BaseHelper
{
    /**
     * @param $object
     * @param array $properties
     * @return mixed
     */
    public static function configure($object, array $properties = [])
    {
        foreach ($properties as $name => $value) {
            $object->$name = $value;
        }

        return $object;
    }
}
