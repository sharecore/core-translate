<?php

namespace core\translate;

use GuzzleHttp\Client;
use GuzzleHttp\Cookie\CookieJar;
use Psr\Http\Message\ResponseInterface;

class RequestBuilder
{
    /**
     * @var string
     */
    protected $_url;

    /**
     * @var array
     */
    protected $_params;

    /**
     * @var array
     */
    protected $_posts;

    /**
     * @var array
     */
    protected $_files;

    /**
     * @var array
     */
    protected $_cookies;

    /**
     * @var array
     */
    protected $_json;

    /**
     * @var mixed
     */
    protected $_body;

    /**
     * @var string[]
     */
    protected $_headers;

    /**
     * @var mixed
     */
    protected $_auth;

    /**
     * @var ResponseInterface
     */
    protected $_httpResponse;

    /**
     * @param string $url
     */
    public function __construct(string $url)
    {
        $this->_url = $url;

        $this->_headers = [];
        $this->_params = [];
        $this->_posts = [];
        $this->_files = [];
        $this->_cookies = [];
    }

    /**
     * @param string $key
     * @param $value
     * @return $this
     */
    public function addParam(string $key, $value): RequestBuilder
    {
        $this->_params[$key] = $value;
        return $this;
    }

    /**
     * @param $key
     * @param $value
     * @return $this
     */
    public function addPost($key, $value): RequestBuilder
    {
        $this->_posts[$key] = $value;
        return $this;
    }

    /**
     * @param array $posts
     * @return $this
     */
    public function addPosts(array $posts): RequestBuilder
    {
        foreach ($posts as $key => $value) {
            $this->addPost($key, $value);
        }
        return $this;
    }

    /**
     * @param string $key
     * @param string $data
     * @param string $filename
     * @return $this
     */
    public function addFileData(string $key, $data, string $filename): RequestBuilder
    {
        $this->_files[$key] = [
            'name' => $key,
            'contents' => $data,
            'filename' => $filename,
        ];

        return $this;
    }

    /**
     * @param string $key
     * @param $value
     * @return $this
     */
    public function addHeader(string $key, $value): RequestBuilder
    {
        $this->_headers[$key] = $value;
        return $this;
    }

    /**
     * @param array $cookies
     * @return $this
     */
    public function setCookies(array $cookies): RequestBuilder
    {
        $this->_cookies = $cookies;
        return $this;
    }

    /**
     * @param mixed $auth
     * @return $this
     */
    public function setAuth($auth): RequestBuilder
    {
        $this->_auth = $auth;
        return $this;
    }

    /**
     * @param array $json
     * @return $this
     */
    public function setJsons(array $json): RequestBuilder
    {
        $this->_json = $json;
        return $this;
    }

    /**
     * @param mixed $body
     * @return RequestBuilder
     */
    public function setBody($body): RequestBuilder
    {
        $this->_body = $body;
        return $this;
    }

    /**
     * @return mixed|ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function _buildRequest()
    {
        $client = new Client();
        $cookieJar = new CookieJar(false, $this->_cookies);

        $method = ($this->_files || $this->_posts || $this->_json) ? 'POST' : 'GET';

        $options = [];

        if ($this->_params) {
            $options['query'] = $this->_params;
        }

        if ($this->_posts && !$this->_files) {
            $options['form_params'] = $this->_posts;
        }

        if ($this->_files) {
            $options['multipart'] = $this->_files;
            if ($this->_posts) {
                foreach ($this->_posts as $key => $value) {
                    $options['multipart'][] = [
                        'name' => $key,
                        'contents' => $value,
                    ];
                }
            }
        }

        if ($this->_json) {
            $options['json'] = $this->_json;
        }

        if ($this->_body) {
            $options[] = is_array($this->_body) ? json_encode($this->_body) : $this->_body;
        }

        if ($this->_auth) {
            $options['auth'] = $this->_auth;
        }

        $options['headers'] = $this->_headers;
        $options['cookies'] = $cookieJar;

        $response = $client->request($method, $this->_url, $options);

        $this->_cookies = $cookieJar->toArray();

        return $response;
    }

    /**
     * @return ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getHttpResponse(): ResponseInterface
    {
        if ($this->_httpResponse === null) {
            $this->_httpResponse = $this->_buildRequest();
        }

        return $this->_httpResponse;
    }

    /**
     * @return string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getResponseBody(): string
    {
        $httpResponse = $this->getHttpResponse();
        return (string)$httpResponse->getBody();
    }

    /**
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getResponseJson()
    {
        $rawResponse = $this->getResponseBody();
        return json_decode($rawResponse, true);
    }

    /**
     * @param string $mapClass
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function map(string $mapClass)
    {
        return new $mapClass($this->getResponseJson());
    }
}
