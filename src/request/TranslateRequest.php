<?php

namespace core\translate\request;

use core\translate\BaseObject;

class TranslateRequest extends BaseObject
{
    /**
     * @var string
     */
    public $text;

    /**
     * @var string
     */
    public $form;

    /**
     * @var string
     */
    public $to;
}