<?php

namespace core\translate;

use core\translate\exception\TranslateException;
use core\translate\request\TranslateRequest;
use core\translate\response\TranslateResponse;

interface ProviderInterface
{
    /**
     * @param TranslateRequest $request
     * @return TranslateResponse
     */
    public function translate(TranslateRequest $request): TranslateResponse;

    /**
     * @param \Exception $e
     * @throws TranslateException
     */
    public function handleException(\Exception $e): void;
}