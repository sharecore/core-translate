<?php

namespace core\translate;

use LazyJsonMapper\LazyJsonMapper;

class BaseMapper extends LazyJsonMapper
{
    public const ALLOW_VIRTUAL_PROPERTIES = false;
    public const ALLOW_VIRTUAL_FUNCTIONS = true;
    public const USE_MAGIC_LOOKUP_CACHE = true;
}
