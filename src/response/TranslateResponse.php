<?php

namespace core\translate\response;

use core\translate\BaseObject;

class TranslateResponse extends BaseObject
{
    /**
     * @var string
     */
    public $text;
}