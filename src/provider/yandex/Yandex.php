<?php

namespace core\translate\provider\yandex;

use core\translate\BaseObject;
use core\translate\exception\TranslateException;
use core\translate\provider\yandex\helper\YandexHelper;
use core\translate\provider\yandex\response\TranslateMapperResponse;
use core\translate\ProviderInterface;
use core\translate\request\TranslateRequest;
use core\translate\RequestBuilder;
use core\translate\response\TranslateResponse;

class Yandex extends BaseObject implements ProviderInterface
{
    /**
     * @var string
     */
    public $key;

    /**
     * @param TranslateRequest $request
     * @return TranslateResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function translate(TranslateRequest $request): TranslateResponse
    {
        $response = $this->request('https://translate.yandex.net/api/v1.5/tr.json/translate')
            ->addPost('key', $this->key)
            ->addPost('text', $request->text)
            ->addPost('lang', $request->form . '-' . $request->to)
            ->getResponseJson();

        $translateMapperResponse = new TranslateMapperResponse($response);

        return new TranslateResponse([
            'text' => YandexHelper::getText($translateMapperResponse)
        ]);
    }

    /**
     * @param string $url
     * @return RequestBuilder
     */
    private function request(string $url): RequestBuilder
    {
        return new RequestBuilder($url);
    }

    /**
     * @param \Exception $e
     * @throws TranslateException
     */
    public function handleException(\Exception $e): void
    {
        throw new TranslateException('Unknown error', 0, $e);
    }
}