<?php

namespace core\translate\provider\yandex\helper;

use core\translate\provider\yandex\response\TranslateMapperResponse;

class YandexHelper
{
    /**
     * @param TranslateMapperResponse $response
     * @return string
     */
    public static function getText(TranslateMapperResponse $response): string
    {
        return is_array($response->getText()) ? $response->getText()[0] : '';
    }
}
