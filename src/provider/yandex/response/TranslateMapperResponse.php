<?php

namespace core\translate\provider\yandex\response;

use core\translate\BaseMapper;

/**
 * @method bool isCode()
 * @method string getCode()
 * @method bool isLang()
 * @method string getLang()
 * @method bool isText()
 * @method string[] getText()
 */
class TranslateMapperResponse extends BaseMapper
{
    public const JSON_PROPERTY_MAP = [
        'code' => 'int',
        'lang' => 'int',
        'text' => '[]',
    ];
}
