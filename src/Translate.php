<?php

namespace core\translate;

use core\translate\exception\TranslateException;
use core\translate\helper\BaseHelper;
use core\translate\request\TranslateRequest;
use core\translate\response\TranslateResponse;

class Translate
{
    public const LANGUAGE_RU = 'ru';
    public const LANGUAGE_EN = 'en';

    /**
     * @var ProviderInterface
     */
    private $provider;

    /**
     * @param string $providerClass
     * @param array $params
     */
    public function __construct(string $providerClass, array $params)
    {
        $provider = self::buildTranslateProvider($providerClass, $params);
        $this->provider = $provider;
    }

    /**
     * @param TranslateRequest $request
     * @return TranslateResponse
     * @throws exception\TranslateException
     */
    public function translate(TranslateRequest $request): TranslateResponse
    {
        try {
            return $this->provider->translate($request);
        } catch (\Exception $e) {
            $this->handleException($e);
        }
    }

    /**
     * @param \Exception $e
     * @throws exception\TranslateException
     */
    private function handleException(\Exception $e): void
    {
        $this->provider->handleException($e);
        throw new TranslateException('Something went wrong', 0, $e);
    }

    /**
     * @param string $providerClass
     * @param array $params
     * @return ProviderInterface
     */
    private static function buildTranslateProvider(string $providerClass, array $params): ProviderInterface
    {
        /** @var ProviderInterface $provider */
        $provider = new $providerClass($params);
        BaseHelper::configure($provider, $params);
        return $provider;
    }
}