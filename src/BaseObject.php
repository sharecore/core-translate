<?php

namespace core\translate;

use core\translate\helper\BaseHelper;

class BaseObject
{
    /**
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        if (!empty($config)) {
            BaseHelper::configure($this, $config);
        }
    }
}
