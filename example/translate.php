<?php

require __DIR__ . '/define.php';

use core\translate\provider\yandex\Yandex;
use core\translate\request\TranslateRequest;
use core\translate\Translate;

$translate = new Translate(Yandex::class, [
    'key' => 'trnsl.1.1.20190719T021434Z.d3f1d9928dc69908.21562cbf924e1383aca8e5f010616a2d0d88fbf4'
]);

$translateResponse = $translate->translate(new TranslateRequest([
    'text' => 'Привет мир!',
    'form' => Translate::LANGUAGE_EN,
    'to' => Translate::LANGUAGE_RU
]));

print_r($translateResponse);